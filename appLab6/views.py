from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .forms import statusForm
from .models import statusModel
from django.urls import reverse

# Create your views here.
def myStatus(request):	#simpan ke database
	response = {'form': statusForm, 'model': statusModel.objects.all()}
	return render(request, 'status.html', response)

def statusPost(request):	
	if(request.method == 'POST'):
		form = statusForm(request.POST)
		objstatus = statusModel()
		objstatus.isiStatus = request.POST['isiStatus'] 
		objstatus.save()
		return HttpResponseRedirect(reverse('myStatus'))	#statusnya itu htmlnya
	else:
		form = statusForm()
		return HttpResponseRedirect(reverse('myStatus'))

def page1(request):
    response = {}
    return render(request, 'page1story4.html', response)