from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import myStatus
from .views import statusPost

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class testLab6(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab_6_using_to_do_list_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'status.html')

	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = myStatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn("Hello, Apa kabar?", html_response)

	def test_add_url_exist(self):
		response = Client().get('/status')
		self.assertEqual(response.status_code, 302)


# Create your tests here.
class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story7FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium	
        # Opening the link we want to test
        selenium.get('http://localhost:8000')
        # find the form element
        title = selenium.find_element_by_id('id_isiStatus')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        title.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

    def test_button_size(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	tombol = selenium.find_element_by_id('submit')
    	tombol_size = tombol.size
    	self.assertEqual(tombol_size, {'height': 42, 'width': 54})
    	
    def test_button_location(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	tomboll = selenium.find_element_by_id('submit')
    	tomboll_location = tomboll.location
    	self.assertEqual(tomboll_location, {'x': 2, 'y': 446})
    	
    def test_text_css(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	teks = selenium.find_element_by_id('halo')
    	ukuran = teks.value_of_css_property('font-size')
    	align = teks.value_of_css_property('text-align')
    	self.assertEqual(ukuran, '24px')
    	self.assertEqual(align, 'center')

    def test_button_css(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	tombol2 = selenium.find_element_by_id('submit')
    	warna = tombol2.value_of_css_property('background-color')
    	border_radius = tombol2.value_of_css_property('border-radius')
    	self.assertEqual(warna, 'rgba(219, 112, 147, 1)')
    	self.assertEqual(border_radius, '12px')

	
